package com.codeiseasydev.advertising.client.httpcomponents;


/**
 * A name-value pair parameter used as an element of HTTP messages.
 *
 * <pre>
 * parameter               = attribute "=" value
 * attribute               = token
 * value                   = token | quoted-string
 * </pre>
 *
 * @since 4.0
 */
public interface NameValuePair {

    /**
     * Gets the name of this pair.
     *
     * @return the name of this pair, never {@code null}.
     */
    String getName();

    /**
     * Gets the value of this pair.
     *
     * @return the value of this pair, may be {@code null}.
     */
    String getValue();

}