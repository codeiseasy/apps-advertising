package com.codeiseasydev.advertising.client

import com.codeiseasydev.advertising.model.PojoModel

interface HttpClientCallback {
    fun onResponseSuccess(pojo: ArrayList<PojoModel>)
    fun onResponseError(error: String?)
}
