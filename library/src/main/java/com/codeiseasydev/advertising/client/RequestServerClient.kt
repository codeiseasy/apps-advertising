package com.codeiseasydev.advertising.client

import android.content.Context
import com.android.volley.*
import com.android.volley.toolbox.JsonArrayRequest
import com.codeiseasydev.advertising.model.PojoModel
import com.codeiseasydev.advertising.util.LogDebug
import org.json.JSONArray
import org.json.JSONException
import com.codeiseasydev.advertising.client.httpcomponents.URIBuilder
import com.google.gson.Gson
import java.io.*
import java.net.URI
import java.util.regex.Pattern

class RequestServerClient {
    private var context: Context? = null
    private var serverUrl: String? = null
    private var serverPath: String? = ""
    private var queryName: String? = ""
    private var queryValue: String? = ""

    private val htmlPattern = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>"
    private val pattern = Pattern.compile(htmlPattern)

    constructor()
    
    constructor(context: Context?) {
        this.context = context
    }


    fun setUrl(url: String?): RequestServerClient {
        this.serverUrl = url
        return this
    }

    fun setUrl(url: String?, path: String?): RequestServerClient {
        this.serverUrl = url
        this.serverPath = path
        return this
    }

    fun setUrl(url: String?, path: String?, queryName: String?, queryValue: String?): RequestServerClient {
        this.serverUrl = url
        this.serverPath = path
        this.queryName = queryName
        this.queryValue = queryValue
        return this
    }

    fun setPath(path: String?): RequestServerClient {
        this.serverPath = path
        return this
    }

    fun setQuery(query: String?, value: String?): RequestServerClient {
        this.queryName = query
        this.queryValue = value
        return this
    }


    fun fetch(httpClientCallback: HttpClientCallback?) {
        LogDebug.d("URIBuilderLink", hasUrl(serverUrl))
        val request = JsonArrayRequest(
            Request.Method.GET,
            hasUrl(serverUrl), null, Response.Listener<JSONArray> { response ->
            var pojoList = ArrayList<PojoModel>()
            for (i in 0 until response.length()) {
                try {
                    //val jsonObject = response.getJSONObject(i)
                    val pojos = Gson().fromJson(response.get(i).toString(), PojoModel::class.java)
                    pojoList.add(pojos)
                } catch (e: JSONException) {
                    httpClientCallback?.onResponseError(e.message.toString())
                }
            }
            httpClientCallback?.onResponseSuccess(pojoList)
        }, Response.ErrorListener { volleyError ->
                httpClientCallback?.onResponseError(volleyError.message.toString())
        })
        BackendVolley.getInstance(context).addToRequestQueue(request)
    }

    @Throws(IOException::class)
    private fun streamToString(IS: InputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(IS))
        var result = ""
        while (true) {
            var str: String? = bufferedReader.readLine() ?: return result
            result = StringBuilder(result).append(str).toString()
        }
    }

    private fun hasHTMLTags(text: String?): Boolean {
        val matcher = pattern.matcher(text)
        return matcher.find()
    }


    private fun hasUrl(url: String?): String?{
        return if (serverPath!!.isNotBlank() || queryName!!.isNotBlank() || queryValue!!.isNotBlank() ) {
            val uri = URI(url)
            val uriBuilder = URIBuilder()
            uriBuilder.scheme = uri.scheme
            uriBuilder.host = uri.host
            uriBuilder.path = "$serverPath/"
            uriBuilder.setParameter(queryName, queryValue)
            val build = uriBuilder.build()
            build.toURL().toString()
        } else {
            url
        }
    }


    companion object {
        fun builder(ctx: Context?): RequestServerClient {
            return RequestServerClient(ctx)
        }
    }
}