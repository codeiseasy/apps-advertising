package com.codeiseasydev.advertising.listener

interface OnButtonInstallClickListener {
    fun onButtonInstallClick(pkg: String?)
}