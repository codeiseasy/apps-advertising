package com.codeiseasydev.advertising

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Handler

import android.view.Gravity
import android.view.View

import com.codeiseasydev.advertising.adapter.AppsVerticalAdapter
import com.codeiseasydev.advertising.client.HttpClientCallback
import com.codeiseasydev.advertising.listener.OnButtonInstallClickListener

import com.codeiseasydev.advertising.model.PojoModel

import com.codeiseasydev.advertising.util.LogDebug
import com.easycoding.drawing.utility.CheckNetwork


import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.advertising.client.RequestServerClient
import com.codeiseasydev.advertising.enums.DialogMode
import com.codeiseasydev.advertising.util.Utils

import com.codeiseasydev.advertising.util.Utils.dimension
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog


class AdMyAppsDialog {
    private var context: Context? = null
    private var serverUrl: String? = null
    private var serverPath: String? = ""
    private var queryName: String? = ""
    private var queryValue: String? = ""




    constructor(context: Context?, url: String?) {
        this.context = context
        this.serverUrl = url
    }

    constructor(context: Context?) {
        this.context = context
    }

    fun setUrl(url: String?): AdMyAppsDialog {
        this.serverUrl = url
        return this
    }

    fun setPath(path: String?): AdMyAppsDialog {
        this.serverPath = path
        return this
    }

    fun setQuery(query: String?, value: String?): AdMyAppsDialog {
        this.queryName = query
        this.queryValue = value
        return this
    }

    fun build(mode: DialogMode){
        var containerLayout = LinearLayout(context)
        var containerLayoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        containerLayoutParams.topMargin = 5
        containerLayoutParams.leftMargin = 5
        containerLayoutParams.rightMargin = 5
        containerLayoutParams.bottomMargin = 5
        containerLayout.layoutParams = containerLayoutParams
        containerLayout.orientation = LinearLayout.VERTICAL
        containerLayout.gravity = Gravity.CENTER
        containerLayout.setBackgroundResource(R.drawable.bottom_sheet_dialog_bg)


        var pbLoading = ProgressBar(context)
        var pbLoadingParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, dimension(context,50).toInt())
        pbLoadingParams.leftMargin = 50
        pbLoadingParams.rightMargin = 50
        pbLoadingParams.bottomMargin = 50
        pbLoadingParams.topMargin = 50
        pbLoading.layoutParams = pbLoadingParams


        var tvDialogTitle = TextView(context)
        var tvDialogTitleParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvDialogTitle.layoutParams = tvDialogTitleParams
        tvDialogTitle.setBackgroundResource(R.drawable.bottom_sheet_dialog_title_bg)
        tvDialogTitle.textSize = 15f
        tvDialogTitle.setTextColor(context!!.resources.getColor(R.color.dialog_title_text_color))
        tvDialogTitle.text = "Hello, I just found these great apps for you."
        tvDialogTitle.gravity = Gravity.CENTER_HORIZONTAL
        tvDialogTitle.visibility = View.GONE
        tvDialogTitle.setPadding(dimension(context,10).toInt(), dimension(context,10).toInt(), dimension(context,10).toInt(), dimension(context,10).toInt())


        var layoutConnectionError = LinearLayout(context)
        var layoutConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutConnectionErrorParams.bottomMargin = 50
        layoutConnectionErrorParams.topMargin = 50
        layoutConnectionError.layoutParams = layoutConnectionErrorParams
        layoutConnectionError.gravity = Gravity.CENTER_VERTICAL


        var ivConnectionError = ImageView(context)
        var ivConnectionErrorParams = LinearLayout.LayoutParams(dimension(context,80).toInt(), LinearLayout.LayoutParams.WRAP_CONTENT)
        ivConnectionError.layoutParams = ivConnectionErrorParams
        ivConnectionError.setImageResource(R.drawable.ic_wifi_error)


        var tvConnectionError = TextView(context)
        var tvConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvConnectionError.layoutParams = tvConnectionErrorParams
        tvConnectionError.textSize = 16f

        var recyclerView = RecyclerView(context!!)
        var recyclerViewParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT)
        recyclerView.layoutParams = recyclerViewParams

        containerLayout.addView(tvDialogTitle)
        containerLayout.addView(pbLoading)
        layoutConnectionError.addView(ivConnectionError)
        layoutConnectionError.addView(tvConnectionError)
        containerLayout.addView(layoutConnectionError)
        containerLayout.addView(recyclerView)

        var storeAppsAdapter = AppsVerticalAdapter(context)
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.adapter = storeAppsAdapter
        storeAppsAdapter.setOnButtonInstallClickListener(object : OnButtonInstallClickListener {
            override fun onButtonInstallClick(pkg: String?) {
                Utils.openAppInPlayStore(context, pkg)
            }
        })
        if (CheckNetwork.getInstance(context!!).isOnline){
            layoutConnectionError.visibility = View.GONE
            RequestServerClient.builder(context)
                .setUrl(serverUrl)
                .setPath(serverPath)
                .setQuery(queryName, queryValue)
                .fetch(object : HttpClientCallback {
                    override fun onResponseSuccess(pojo: ArrayList<PojoModel>) {
                        LogDebug.d("onResponseSuccess", pojo[0].appName.toString())
                        pbLoading.visibility = View.GONE
                        tvDialogTitle.visibility = View.VISIBLE
                        storeAppsAdapter.setItems(pojo)
                    }

                    override fun onResponseError(error: String?) {
                        LogDebug.d("onResponseError", error.toString())
                        pbLoading.visibility = View.GONE
                    }
                })
        } else {
            tvConnectionError.text = "You have disconnected. check your network connection and try again"
            layoutConnectionError.visibility = View.VISIBLE
            pbLoading.visibility = View.GONE
        }
        Utils.deepChangeTextColor(context, containerLayout as ViewGroup, "ar_font.ttf", 9f)
        when(mode){
            DialogMode.NORMAL -> {
                var dialog = Dialog(context!!, R.style.MyDialogTheme)
                dialog.setContentView(containerLayout)
                dialog.window!!.attributes.windowAnimations = R.style.DialogAnimation
                dialog.show()
            }
            DialogMode.SHEET_BOTTOM -> {
                var bottomSheetDialog = BottomSheetDialog(context!!, R.style.BottomSheetDialogTheme)
                bottomSheetDialog.setContentView(containerLayout)
                bottomSheetDialog.show()

                //val sheetBehavior = BottomSheetBehavior.from(containerLayout.parent as View)
                //val screenUtils = ScreenUtils(context)
                //sheetBehavior.peekHeight = screenUtils.height

                bottomSheetDialog.setOnShowListener(DialogInterface.OnShowListener { dialog ->
                    Handler().postDelayed(Runnable {
                        val d = dialog as BottomSheetDialog
                        val bottomSheet = d.findViewById<FrameLayout>(R.id.design_bottom_sheet)
                        val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
                        bottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    }, 0)
                })
            }
        }

    }

    companion object {

        fun with(context: Context): AdMyAppsDialog{
            return AdMyAppsDialog(context)
        }

        fun with(context: Context, url: String?): AdMyAppsDialog{
            return AdMyAppsDialog(context, url)
        }
    }
}