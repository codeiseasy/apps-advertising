package com.codeiseasydev.advertising.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PojoModel {

    @SerializedName("app_available")
    @Expose
    var appAvailable: Boolean? = null
    @SerializedName("app_name")
    @Expose
    var appName: String? = null
    @SerializedName("app_icon")
    @Expose
    var appIcon: String? = null
    @SerializedName("app_packagename")
    @Expose
    var appPackagename: String? = null
    @SerializedName("app_description")
    @Expose
    var appDescription: String? = null
    @SerializedName("app_updated")
    @Expose
    var appUpdated: String? = null
    @SerializedName("app_size")
    @Expose
    var appSize: String? = null
    @SerializedName("app_current_version")
    @Expose
    var appCurrentVersion: String? = null
    @SerializedName("app_requires_android")
    @Expose
    var appRequiresAndroid: String? = null
    @SerializedName("app_developer_email")
    @Expose
    var appDeveloperEmail: String? = null
    @SerializedName("app_developer_name")
    @Expose
    var appDeveloperName: String? = null
    @SerializedName("app_content_rating")
    @Expose
    var appContentRating: String? = null
    @SerializedName("app_reviews")
    @Expose
    var appReviews: Double? = null

}