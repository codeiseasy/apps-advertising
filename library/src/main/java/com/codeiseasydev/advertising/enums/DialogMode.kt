package com.codeiseasydev.advertising.enums

enum class DialogMode {
    NORMAL,
    SHEET_BOTTOM
}