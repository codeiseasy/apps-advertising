package com.codeiseasydev.advertising.adapter

import android.content.Context

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.codeiseasydev.advertising.model.PojoModel
import com.codeiseasydev.advertising.util.Utils
import xyz.schwaab.avvylib.AvatarView

import android.graphics.Bitmap
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.assist.ImageSize
import com.nostra13.universalimageloader.core.DisplayImageOptions
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener


open class AppsHorizontalAdapter: RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private var ctx: Context? = null
    private var resId: Int = com.codeiseasydev.advertising.R.layout.horizontal_row_ad_items
    private var pojoList: ArrayList<PojoModel> = ArrayList()


    constructor(context: Context?)  : super() {
        this.ctx = context
    }

    constructor(context: Context?, @LayoutRes resId: Int)  : super() {
        this.ctx = context
        this.resId = resId
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var civAppIcon: AvatarView = itemView.findViewById<AvatarView>(com.codeiseasydev.advertising.R.id.avAppIcon)
        var tvAppName: TextView = itemView.findViewById<TextView>(com.codeiseasydev.advertising.R.id.tvAppName)
        var tvAdBadge: TextView = itemView.findViewById<TextView>(com.codeiseasydev.advertising.R.id.tv_ad_badge)

        init {
            Utils.deepChangeTextColor(itemView.context, itemView as ViewGroup, "ar_font.ttf", 9f)

            civAppIcon.apply {
                isAnimating = true
                borderThickness = 5
                highlightBorderColor = itemView.context.resources.getColor(com.codeiseasydev.advertising.R.color.pink400)
                highlightBorderColorEnd = itemView.context.resources.getColor(com.codeiseasydev.advertising.R.color.pinkA700)
                distanceToBorder = 6
                numberOfArches = 5
                totalArchesDegreeArea = 90f
            }
        }
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(resId, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var pojo = pojoList[position]
        var holder = viewHolder as ViewHolder
        holder.tvAppName.text = pojo.appName



        var targetSize = ImageSize(80, 50) // result Bitmap will be fit to this size

        val imageLoader = ImageLoader.getInstance()
        imageLoader.init(ImageLoaderConfiguration.createDefault(holder.itemView.context))
        var options = DisplayImageOptions.Builder()
            //.showImageOnLoading(R.drawable.stat_notify_error)
            //.showImageForEmptyUri(R.drawable.stat_notify_error)
            //.showImageOnFail(R.drawable.stat_notify_error)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .build()


        imageLoader.displayImage(pojo.appIcon, holder.civAppIcon, options, object : ImageLoadingListener {
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                holder.civAppIcon.apply {
                    isAnimating = false
                }
            }

            override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {

            }

            override fun onLoadingStarted(imageUri: String?, view: View?) {

            }

            override fun onLoadingCancelled(imageUri: String?, view: View?) {

            }
        })


       /* ImageLoader(holder.itemView.context).displayImage(pojo.appIcon, 0,  holder.civAppIcon, object : ImageLoaderCallback {
            override fun onSuccess() {
                holder.civAppIcon.apply {
                    isAnimating = false
                }
            }

            override fun onError() {
                holder.civAppIcon.apply {
                    isAnimating = false
                }
            }
        })*/
        holder.civAppIcon.setOnClickListener {
            Utils.openAppInPlayStore(ctx, pojo.appPackagename)
        }
    }


    fun setItems(items: ArrayList<PojoModel>){
        for (item in items){
            add(item)
        }
    }

    fun setItems(items: PojoModel){
         add(items)
    }

     private fun add(item: PojoModel){
        if (item.appAvailable!! && item.appPackagename != ctx!!.packageName){
            pojoList.add(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return pojoList.size
    }
}