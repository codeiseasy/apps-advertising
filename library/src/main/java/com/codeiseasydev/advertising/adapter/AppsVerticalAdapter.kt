package com.codeiseasydev.advertising.adapter

import android.content.Context
import android.graphics.Bitmap
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.codeiseasydev.advertising.R
import com.codeiseasydev.advertising.listener.OnButtonInstallClickListener
import com.codeiseasydev.advertising.model.PojoModel
import com.codeiseasydev.advertising.util.Utils
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener

class AppsVerticalAdapter(private val ctx: Context?) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var pojoModelList: ArrayList<PojoModel> = ArrayList()
    private var btnInstallClickListener: OnButtonInstallClickListener? = null

    fun setOnButtonInstallClickListener(listener: OnButtonInstallClickListener){
        btnInstallClickListener = listener
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       // var cardView: CardView = itemView.findViewById<CardView>(R.id.cardView)
        var progressBar: ProgressBar = itemView.findViewById<ProgressBar>(R.id.progressBar)
        var ivAppIcon: ImageView = itemView.findViewById<ImageView>(R.id.ivAppIcon)
        var tvAppName: TextView = itemView.findViewById<TextView>(R.id.tvAppName)
        var tvAppDeveloperName: TextView = itemView.findViewById<TextView>(R.id.tvAppDeveloperName)
        var rbAppReview: RatingBar = itemView.findViewById<RatingBar>(R.id.rbAppReview)
        var bnAppInstall: Button = itemView.findViewById<Button>(R.id.bnAppInstall)

        init {
            Utils.deepChangeTextColor(itemView.context, itemView as ViewGroup, "ar_font.ttf", 9f)
            bnAppInstall.setOnClickListener {
                var pojoModel = pojoModelList[adapterPosition]
                if (pojoModel.appPackagename != null) {
                    btnInstallClickListener?.onButtonInstallClick(pojoModel.appPackagename)
                }
            }
            itemView.setOnClickListener {
                var pojoModel = pojoModelList[adapterPosition]
                if (pojoModel.appPackagename != null) {
                    btnInstallClickListener?.onButtonInstallClick(pojoModel.appPackagename)
                }
            }
        }
    }


    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(viewGroup.context).inflate(R.layout.vertical_row_ad_items, viewGroup, false))
    }

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        var pojoModel = pojoModelList[position]
        var holder = viewHolder as ViewHolder
        holder.tvAppName.text = pojoModel.appName
        holder.tvAppDeveloperName.text = pojoModel.appDeveloperName
        holder.rbAppReview.rating = pojoModel.appReviews!!.toFloat()


        val imageLoader = ImageLoader.getInstance()
        imageLoader.init(ImageLoaderConfiguration.createDefault(holder.itemView.context))
        var options = DisplayImageOptions.Builder()
            //.showImageOnLoading(R.drawable.stat_notify_error)
            //.showImageForEmptyUri(R.drawable.stat_notify_error)
            //.showImageOnFail(R.drawable.stat_notify_error)
            .cacheInMemory(true)
            .cacheOnDisk(true)
            .considerExifParams(true)
            .bitmapConfig(Bitmap.Config.RGB_565)
            .build()


        imageLoader.displayImage(pojoModel.appIcon, holder.ivAppIcon, options, object :
            ImageLoadingListener {
            override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                holder.progressBar.visibility = View.GONE
            }

            override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {

            }

            override fun onLoadingStarted(imageUri: String?, view: View?) {

            }

            override fun onLoadingCancelled(imageUri: String?, view: View?) {

            }
        })
    }


    fun setItems(items: ArrayList<PojoModel>){
        for (item in items){
            add(item)
        }
    }

    fun setItems(items: PojoModel){
         add(items)
    }

     private fun add(item: PojoModel){
        if (item.appAvailable!! && item.appPackagename != ctx!!.packageName){
            pojoModelList.add(item)
            notifyDataSetChanged()
        }
    }

    override fun getItemCount(): Int {
        return pojoModelList.size
    }
}