package com.codeiseasydev.advertising

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class AdMainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.vertical_row_ad_items)
    }
}
