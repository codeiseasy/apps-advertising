package com.codeiseasy.advertising

import android.graphics.Bitmap
import android.graphics.Color

import android.os.Bundle

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView

import com.codeiseasydev.advertising.AdMyAppsDialog
import com.codeiseasydev.advertising.adapter.AppsHorizontalAdapter
import com.codeiseasydev.advertising.client.HttpClientCallback
import com.codeiseasydev.advertising.client.RequestServerClient
import com.codeiseasydev.advertising.enums.DialogMode

import com.codeiseasydev.advertising.model.PojoModel
import com.codeiseasydev.advertising.util.LogDebug
import com.codeiseasydev.advertising.util.Utils
import com.codeiseasydev.advertising.view.BadgeOval
import com.easycoding.drawing.utility.CheckNetwork
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private var baseURL =  "https://iamdeveloper.herokuapp.com/" //"https://your_url.com"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        buildBanner()
        appsListHorizontalRecyclerView()
        bnAppsListDialog.setOnClickListener{appsListDialog()}
    }
    

    private fun buildBanner(){
        var view = LayoutInflater.from(this).inflate(R.layout.vertical_row_ad_items, null)
        Utils.deepChangeTextColor(this, view as ViewGroup, "ar_font.ttf")
        var viewParams = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        var cardView = view.findViewById<RelativeLayout>(R.id.cardView)
        var progressBar = view.findViewById<ProgressBar>(R.id.progressBar)
        var ivAppIcon = view.findViewById<ImageView>(R.id.ivAppIcon)
        var tvAppName = view.findViewById<TextView>(com.codeiseasydev.advertising.R.id.tvAppName)
        var tvAppDeveloperName = view.findViewById<TextView>(com.codeiseasydev.advertising.R.id.tvAppDeveloperName)
        var rbAppReview = view.findViewById<RatingBar>(com.codeiseasydev.advertising.R.id.rbAppReview)
        var bnAppInstall = view.findViewById<Button>(com.codeiseasydev.advertising.R.id.bnAppInstall)


        var layoutConnectionError = LinearLayout(this)
        var layoutConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        layoutConnectionErrorParams.bottomMargin = 50
        layoutConnectionErrorParams.topMargin = 50
        layoutConnectionError.layoutParams = layoutConnectionErrorParams
        layoutConnectionError.gravity = Gravity.CENTER_VERTICAL
        layoutConnectionError.setPadding(
            Utils.dimension(this, 0).toInt(),
            Utils.dimension(this, 10).toInt(),
            Utils.dimension(this, 0).toInt(),
            Utils.dimension(this, 10).toInt()
        )

        var ivConnectionError = ImageView(this)
        var ivConnectionErrorParams = LinearLayout.LayoutParams(Utils.dimension(this, 80).toInt(), LinearLayout.LayoutParams.WRAP_CONTENT)
        ivConnectionError.layoutParams = ivConnectionErrorParams
        ivConnectionError.setImageResource(com.codeiseasydev.advertising.R.drawable.ic_wifi_error)


        var tvConnectionError = TextView(this)
        var tvConnectionErrorParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)
        tvConnectionError.layoutParams = tvConnectionErrorParams
        tvConnectionError.textSize = 16f

        layoutConnectionError.addView(ivConnectionError)
        layoutConnectionError.addView(tvConnectionError)

        RequestServerClient.builder(this)
            .setUrl(baseURL)
            .setPath("apps")
            .setQuery("type", "json")
            .fetch(object : HttpClientCallback {
                override fun onResponseSuccess(pojo: ArrayList<PojoModel>) {
                    LogDebug.d("onResponseSuccess", pojo[0].appName.toString())
                    var listLength = pojo.size
                    var list = pojo

                    var random = java.util.Random()
                    var randomNext = random.nextInt(listLength)
                    var pojo = list[randomNext]

                    tvAppName.text = pojo.appName
                    tvAppDeveloperName.text = pojo.appDeveloperName
                    rbAppReview.rating = pojo.appReviews!!.toFloat()

                    val imageLoader = ImageLoader.getInstance()
                    imageLoader.init(ImageLoaderConfiguration.createDefault(this@MainActivity))
                    var options = DisplayImageOptions.Builder()
                        //.showImageOnLoading(R.drawable.stat_notify_error)
                        //.showImageForEmptyUri(R.drawable.stat_notify_error)
                        //.showImageOnFail(R.drawable.stat_notify_error)
                        .cacheInMemory(true)
                        .cacheOnDisk(true)
                        .considerExifParams(true)
                        .bitmapConfig(Bitmap.Config.RGB_565)
                        .build()


                    imageLoader.displayImage(pojo.appIcon, ivAppIcon, options, object :
                        ImageLoadingListener {
                        override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {
                            progressBar.visibility = View.GONE
                        }

                        override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {

                        }

                        override fun onLoadingStarted(imageUri: String?, view: View?) {

                        }

                        override fun onLoadingCancelled(imageUri: String?, view: View?) {

                        }
                    })

                    bnAppInstall.setOnClickListener {
                        Utils.openAppInPlayStore(this@MainActivity, pojo.appPackagename)
                    }
                    adAppView.addView(view, viewParams)
                }

                override fun onResponseError(error: String?) {
                    LogDebug.d("onResponseError", error.toString())
                    if (CheckNetwork.getInstance(this@MainActivity).isOffline){
                        tvConnectionError.text = resources.getString(R.string.error_connection)
                        adAppView.addView(layoutConnectionError, viewParams)
                    }
                }
            })
    }


    private fun appsListDialog(){
        AdMyAppsDialog.with(this)
            .setUrl(baseURL)
            .setPath("apps")
            .setQuery("type", "json")
            .build(DialogMode.SHEET_BOTTOM)
    }

    private fun appsListHorizontalRecyclerView(){
        var rvMoreApps = object : AppsHorizontalAdapter(this) {
            override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
                super.onBindViewHolder(viewHolder, position)
                var holder = viewHolder as ViewHolder
                //holder.tvAppName.setTextColor(Color.BLUE)
                holder.tvAdBadge.setBackgroundDrawable(
                    BadgeOval(this@MainActivity)
                        .setBadgeBackgroundColor(Color.WHITE)
                        .setStrokeWidth(2f).setStrokeColors(
                            intArrayOf(
                                resources.getColor(com.codeiseasydev.advertising.R.color.pink400),
                                resources.getColor(com.codeiseasydev.advertising.R.color.pinkA700))
                        )
                        .draw())
            }
        }

        RequestServerClient.builder(this)
            .setUrl(baseURL)
            .setPath("apps")
            .setQuery("type", "json")
            .fetch(object : HttpClientCallback {
                override fun onResponseSuccess(pojo: ArrayList<PojoModel>) {
                    LogDebug.d("onResponseSuccess", pojo[0].appName.toString())
                    rvMoreApps.setItems(pojo)
                }

                override fun onResponseError(error: String?) {
                    LogDebug.d("onResponseError", error.toString())
                }
            })
        rv_more_apps.adapter = rvMoreApps
    }

}
